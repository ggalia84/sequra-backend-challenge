class CreateDisbursements < ActiveRecord::Migration[5.2]
  def change
    create_table :disbursements do |t|
      t.references :merchant, foreign_key: true
      t.integer :amount
      t.datetime :start_week
      t.datetime :end_week

      t.timestamps
    end
  end
end
