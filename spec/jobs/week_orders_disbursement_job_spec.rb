require 'rails_helper'

RSpec.describe WeekOrdersDisbursementJob, type: :job do
  describe '#perform_later' do
    it 'specify that job was enqueued ' do
      ActiveJob::Base.queue_adapter = :test
      expect { WeekOrdersDisbursementJob.perform_later }.to have_enqueued_job
    end
  end
end
