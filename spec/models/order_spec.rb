require 'rails_helper'

RSpec.describe Order, type: :model do
  let(:merchant) { Merchant.new(name: 'Windler and Sons', email: 'info@windler-and-sons.com', cif: 'B611111112') }
  let(:shopper) { Shopper.new(name: 'Olive Thompson', email: 'olive.thompson@not_gmail.com', nif: '411111111Z') }
  let(:order) { Order.new(merchant: merchant, shopper: shopper, amount: 1234) }

  describe 'when checking accept validations' do
    it 'accepts a valid order' do
      expect(order.valid?).to be true
      expect(order.errors.empty?).to be true
    end
  end

  describe 'when checking reject validations' do
    it 'rejects order without merchant' do
      order.merchant = nil
      expect(order.valid?).to be false
      expect(order.errors['merchant'].present?).to be true
    end
    it 'rejects order without shopper' do
      order.shopper = nil
      expect(order.valid?).to be false
      expect(order.errors['shopper'].present?).to be true
    end
    it 'rejects order without amount' do
      order.amount = nil
      expect(order.valid?).to be false
      expect(order.errors['amount'].present?).to be true
    end
  end

  describe 'when checking associations order' do
    it 'belongs_to to a merchant' do
      expect(Order.reflect_on_association(:merchant).macro).to eq(:belongs_to)
    end
    it 'belongs_to to a shopper' do
      expect(Order.reflect_on_association(:shopper).macro).to eq(:belongs_to)
    end
  end

  describe 'checking calculate_disbursement method' do
    it 'when amount is under 50 EUR or 5000 cent' do
      order.amount = 4000
      expect(order.calculate_disbursement).to eq(40)
    end
    it 'when amount is equal 50 EUR or 5000 cent' do
      order.amount = 5000
      expect(order.calculate_disbursement).to eq(47)
    end
    it 'when amount is between 50 EUR and 300 EUR' do
      order.amount = 15000
      expect(order.calculate_disbursement).to eq(142)
    end
    it 'when amount is equal 300 EUR or 30000 cent' do
      order.amount = 30000
      expect(order.calculate_disbursement).to eq(285)
    end
    it 'when amount is over 300 EUR or 30000 cent' do
      order.amount = 40000
      expect(order.calculate_disbursement).to eq(340)
    end
  end
end
