require 'rails_helper'

RSpec.describe Disbursement, type: :model do
  let(:start_week) { (DateTime.now - 1.week).beginning_of_week }
  let(:end_week) { start_week.end_of_week }
  let(:merchant) { Merchant.new(name: 'Windler and Sons', email: 'info@windler-and-sons.com', cif: 'B611111112') }
  let(:disbursement) { Disbursement.new(merchant: merchant, amount: 123, start_week: start_week, end_week: end_week) }
  let(:shopper) { Shopper.new(name: 'Olive Thompson', email: 'olive.thompson@not_gmail.com', nif: '411111111Z') }
  let(:merchant_a) { Merchant.new(name: 'Mraz and Sons', email: 'info@mraz-and-sons.com   ', cif: 'B611111113') }
  let(:merchant_b) { Merchant.new(name: 'Cummerata LLC', email: 'info@cummerata-llc.com', cif: 'B611111114') }
  let(:order_1) { Order.new(merchant: merchant_a, shopper: shopper, amount: 5000, completed_at: start_week + 1.day) }
  let(:order_2) { Order.new(merchant: merchant_a, shopper: shopper, amount: 4536, completed_at: start_week + 1.day) }
  let(:order_3) { Order.new(merchant: merchant_b, shopper: shopper, amount: 12089, completed_at: start_week + 1.day) }
  let(:order_4) { Order.new(merchant: merchant_b, shopper: shopper, amount: 100011, completed_at: start_week + 1.day) }
  let(:order_5) { Order.new(merchant: merchant_a, shopper: shopper, amount: 189, completed_at: start_week - 1.day) }
  let(:order_6) { Order.new(merchant: merchant_b, shopper: shopper, amount: 1001, completed_at: start_week - 1.day) }
  let(:order_7) { Order.new(merchant: merchant_a, shopper: shopper, amount: 12569) }
  let(:order_8) { Order.new(merchant: merchant_b, shopper: shopper, amount: 1141) }

  describe 'when checking accept validations' do
    it 'accepts a valid disbursement' do
      expect(disbursement.valid?).to be true
      expect(disbursement.errors.empty?).to be true
    end
  end

  describe 'when checking reject validations' do
    it 'rejects disbursement without merchant' do
      disbursement.merchant = nil
      expect(disbursement.valid?).to be false
      expect(disbursement.errors['merchant'].present?).to be true
    end
    it 'rejects disbursement without amount' do
      disbursement.amount = nil
      expect(disbursement.valid?).to be false
      expect(disbursement.errors['amount'].present?).to be true
    end
    it 'rejects disbursement without start_week' do
      disbursement.start_week = nil
      expect(disbursement.valid?).to be false
      expect(disbursement.errors['start_week'].present?).to be true
    end
    it 'rejects disbursement without end_week' do
      disbursement.end_week = nil
      expect(disbursement.valid?).to be false
      expect(disbursement.errors['end_week'].present?).to be true
    end
  end

  describe 'when checking associations disbursement' do
    it 'belongs_to to a merchant' do
      expect(Disbursement.reflect_on_association(:merchant).macro).to eq(:belongs_to)
    end
  end

  describe 'when checking method save_between_dates' do
    it 'expect save nothing disbursement for 0 orders' do
      merchant_a.save
      expect(Merchant.count).to eq(1)
      expect(Shopper.count).to eq(0)
      expect(Order.count).to eq(0)
      Disbursement.save_between_dates(start_week, end_week)
      expect(Disbursement.count).to eq(0)
    end
    it 'expect save nothing disbursement for 1 orders completed out of week' do
      merchant_a.save
      shopper.save
      order_5.save
      expect(Merchant.count).to eq(1)
      expect(Shopper.count).to eq(1)
      expect(Order.count).to eq(1)
      Disbursement.save_between_dates(start_week, end_week)
      expect(Disbursement.count).to eq(0)
    end
    it 'expect save nothing disbursement for 1 orders uncompleted' do
      merchant_a.save
      shopper.save
      order_7.save
      expect(Merchant.count).to eq(1)
      expect(Shopper.count).to eq(1)
      expect(Order.count).to eq(1)
      Disbursement.save_between_dates(start_week, end_week)
      expect(Disbursement.count).to eq(0)
    end
    it 'expect save the disbursement for 1 merchant and 1 order' do
      merchant_a.save
      shopper.save
      order_1.save
      expect(Merchant.count).to eq(1)
      expect(Shopper.count).to eq(1)
      expect(Order.count).to eq(1)
      Disbursement.save_between_dates(start_week, end_week)
      expect(Disbursement.count).to eq(1)
      expect(Disbursement.last.amount).to eq(47)
    end
    it 'expect save the disbursement for 1 merchant and 2 orders' do
      merchant_a.save
      shopper.save
      order_1.save
      order_2.save
      expect(Merchant.count).to eq(1)
      expect(Shopper.count).to eq(1)
      expect(Order.count).to eq(2)
      Disbursement.save_between_dates(start_week, end_week)
      expect(Disbursement.count).to eq(1)
      expect(Disbursement.last.amount).to eq(92)
    end
    it 'expect save the disbursement for 2 merchants' do
      merchant_a.save
      merchant_b.save
      shopper.save
      order_1.save
      order_2.save
      order_3.save
      order_4.save
      expect(Merchant.count).to eq(2)
      expect(Shopper.count).to eq(1)
      expect(Order.count).to eq(4)
      Disbursement.save_between_dates(start_week, end_week)
      expect(Disbursement.count).to eq(2)
      expect(Disbursement.first.amount).to eq(92)
      expect(Disbursement.last.amount).to eq(964)
    end
    it 'expect save the disbursement for 2 merchants and orders completed out of week' do
      merchant_a.save
      merchant_b.save
      shopper.save
      order_1.save
      order_2.save
      order_3.save
      order_4.save
      order_5.save
      order_6.save
      expect(Merchant.count).to eq(2)
      expect(Shopper.count).to eq(1)
      expect(Order.count).to eq(6)
      Disbursement.save_between_dates(start_week, end_week)
      expect(Disbursement.count).to eq(2)
      expect(Disbursement.first.amount).to eq(92)
      expect(Disbursement.last.amount).to eq(964)
    end
    it 'expect save the disbursement for 2 merchants and orders not completed' do
      merchant_a.save
      merchant_b.save
      shopper.save
      order_1.save
      order_2.save
      order_3.save
      order_4.save
      order_7.save
      order_8.save
      expect(Merchant.count).to eq(2)
      expect(Shopper.count).to eq(1)
      expect(Order.count).to eq(6)
      Disbursement.save_between_dates(start_week, end_week)
      expect(Disbursement.count).to eq(2)
      expect(Disbursement.first.amount).to eq(92)
      expect(Disbursement.last.amount).to eq(964)
    end
  end
end
