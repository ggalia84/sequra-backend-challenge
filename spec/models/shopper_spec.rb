require 'rails_helper'

RSpec.describe Shopper, type: :model do
  let(:shopper) { Shopper.new(name: 'Olive Thompson', email: 'olive.thompson@not_gmail.com', nif: '411111111Z') }

  describe 'when checking accept validations' do
    it 'accepts a valid shopper' do
      expect(shopper.valid?).to be true
      expect(shopper.errors.empty?).to be true
    end
  end

  describe 'when checking reject validations' do
    it 'rejects shopper without name' do
      shopper.name = nil
      expect(shopper.valid?).to be false
      expect(shopper.errors['name'].present?).to be true
    end
    it 'rejects shopper without email' do
      shopper.email = nil
      expect(shopper.valid?).to be false
      expect(shopper.errors['email'].present?).to be true
    end
    it 'rejects shopper without nif' do
      shopper.nif = nil
      expect(shopper.valid?).to be false
      expect(shopper.errors['nif'].present?).to be true
    end
    it 'rejects email uniqueness' do
      expect(shopper.valid?).to be true
      shopper.save
      new_shopper = Shopper.new(name: shopper.name, email: shopper.email, nif: '12345678Z')
      expect(new_shopper.valid?).to be false
      expect(new_shopper.errors['email'].present?).to be true
    end
    it 'rejects nif uniqueness' do
      expect(shopper.valid?).to be true
      shopper.save
      new_shopper = Shopper.new(name: shopper.name, email: 'example@email.com', nif: shopper.nif)
      expect(new_shopper.valid?).to be false
      expect(new_shopper.errors['nif'].present?).to be true
    end
  end

  describe 'when checking associations shopper' do
    it 'has_many to a orders' do
      expect(Shopper.reflect_on_association(:orders).macro).to eq(:has_many)
    end
  end
end
