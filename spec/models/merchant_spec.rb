require 'rails_helper'

RSpec.describe Merchant, type: :model do
  let(:merchant) { Merchant.new(name: 'Windler and Sons', email: 'info@windler-and-sons.com', cif: 'B611111112') }

  describe 'when checking accept validations' do
    it 'accepts a valid merchant' do
      expect(merchant.valid?).to be true
      expect(merchant.errors.empty?).to be true
    end
  end

  describe 'when checking reject validations' do
    it 'rejects merchant without name' do
      merchant.name = nil
      expect(merchant.valid?).to be false
      expect(merchant.errors['name'].present?).to be true
    end
    it 'rejects merchant without email' do
      merchant.email = nil
      expect(merchant.valid?).to be false
      expect(merchant.errors['email'].present?).to be true
    end
    it 'rejects merchant without cif' do
      merchant.cif = nil
      expect(merchant.valid?).to be false
      expect(merchant.errors['cif'].present?).to be true
    end
    it 'rejects email uniqueness' do
      expect(merchant.valid?).to be true
      merchant.save
      new_merchant = Merchant.new(name: merchant.name, email: merchant.email, cif: 'B12345678')
      expect(new_merchant.valid?).to be false
      expect(new_merchant.errors['email'].present?).to be true
    end
    it 'rejects cif uniqueness' do
      expect(merchant.valid?).to be true
      merchant.save
      new_merchant = Merchant.new(name: merchant.name, email: 'example@email.com', cif: merchant.cif)
      expect(new_merchant.valid?).to be false
      expect(new_merchant.errors['cif'].present?).to be true
    end
  end

  describe 'when checking associations merchant' do
    it 'has_many to a orders' do
      expect(Merchant.reflect_on_association(:orders).macro).to eq(:has_many)
    end
    it 'has_many to a disbursements' do
      expect(Merchant.reflect_on_association(:disbursements).macro).to eq(:has_many)
    end
  end
end
