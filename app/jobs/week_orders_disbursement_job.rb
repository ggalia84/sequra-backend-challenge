class WeekOrdersDisbursementJob < ApplicationJob
  queue_as :default

  def perform(*args)
    start_week = (DateTime.now - 1.week).beginning_of_week
    end_week = start_week.end_of_week
    Disbursement.save_between_dates(start_week, end_week)
  end
end
