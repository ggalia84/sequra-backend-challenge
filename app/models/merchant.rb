class Merchant < ApplicationRecord
  has_many :orders
  has_many :disbursements
  validates :name, :email, :cif, presence: true
  validates :email, :cif, uniqueness: true
end
