class Order < ApplicationRecord
  belongs_to :merchant
  belongs_to :shopper
  validates :merchant, :shopper, :amount, presence: true

  def calculate_disbursement
    result = 0
    if amount < 5000
      result = (amount * 0.01).to_i
    elsif amount >= 5000 && amount <= 30000
      result = (amount * 0.0095).to_i
    elsif amount > 30000
      result = (amount * 0.0085).to_i
    end
    result
  end
end
