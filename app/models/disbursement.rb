class Disbursement < ApplicationRecord
  belongs_to :merchant
  validates :merchant, :amount, :start_week, :end_week, presence: true

  def self.save_between_dates(start_date, end_date)
    # Get merchants from orders between start and end dates
    Order.where(completed_at: start_date..end_date).distinct.pluck(:merchant_id).each do |merchant|
      # Get orders and sum all disbursements
      disbursement_amount = Order.where(merchant_id: merchant, completed_at: start_date..end_date).to_a.sum(&:calculate_disbursement)
      # Save result in database
      Disbursement.create(merchant_id: merchant, amount: disbursement_amount, start_week: start_date, end_week: end_date)
    end
  end
end
