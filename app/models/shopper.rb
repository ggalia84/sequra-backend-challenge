class Shopper < ApplicationRecord
  has_many :orders
  validates :name, :email, :nif, presence: true
  validates :email, :nif, uniqueness: true
end
